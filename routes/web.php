<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

//Check if user is connected
Route::group(['middleware' => 'auth'], function () {
    //Redirect to the home page
    Route::get('/home', 'HomeController@index')->name('home');
    
    //Redirect to the list with all clients informations
    Route::get('/client/list', 'ClientController@listAllClients')->name('client.list');
    //Redirect to the list with clients informations attach to user
    Route::get('/client/listUser', 'ClientController@listForConnectedUser')->name('client.listUser');
    
    //Redirect to the list with contract informations attach to client
    Route::get('/contract/listClient/{id}', 'ContractController@listContractForClient')->name('contract.listClient');

    Route::get('/product/list/{idContract}/{idClient}', 'ProductController@listProductForContract')->name('product.list');

    Route::get('/calculCa', 'ContractController@listCa')->name('contract.calculCa');
});