<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price'
    ];

    /**
     * The clients that belong to the product.
     */
    public function clients()
    {
        return $this->belongsToMany('App\Client', 'client_contract_product')->withPivot('quantity', 'product_id');
    }
    /**
     * The contracts that belong to the product.
     */
    public function contracts()
    {
        return $this->belongsToMany('App\Contract', 'client_contract_product')->withPivot('quantity', 'client_id');
    }
}
