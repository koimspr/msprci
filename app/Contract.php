<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'number'
    ];
    /**
     * The clients that belong to the contract.
     */
    public function clients()
    {
        return $this->belongsToMany('App\Client');
    }

    /**
     * The products that belong to the contract.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'client_contract_product')->withPivot('quantity', 'client_id');
    }
}
