<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Client extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fiscalName', 'city', 'country', 'contractNumber'
    ];

    /**
     * The users that belong to the client.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * The contracts that belong to the client.
     */
    public function contracts()
    {
        return $this->belongsToMany('App\Contract', 'client_contract_product')->withPivot('quantity', 'product_id');
    }
}
