<?php

namespace App\Http\Controllers;

use App\Product;
use App\Contract;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class ProductController extends Controller
{
    /**
     * Get contracts for a specified user
     *
     * @param [type] $client_id
     * @return void
     */
    public function getProductForUserContract($contract_id, $client_id)
    {
        return Contract::find($contract_id)->products()->get();
    }

    public function listProductForContract($contracts, $client_id)
    {
        $contract = new ContractController;
        $products = self::getProductForUserContract($contracts, $client_id);
        $arrPrice = $contract->calculateTTX($products);
        return view('product.list', compact('products', 'arrPrice', 'client_id'));
    }
}
