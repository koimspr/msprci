<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Contract;

class ContractController extends Controller
{
    /**
     * Get all contract
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllContract()
    {
        return Product::All();
    }

    /**
     * Get contracts for a specified user
     *
     * @param [type] $client_id
     * @return void
     */
    public function getContractForUser($client_id)
    {
        return Client::find($client_id)->contracts;
    }

    public function listContractForClient($client_id)
    {
        $contracts = self::getContractForUser($client_id);
        return view('contract.list', compact('contracts', 'client_id'));
    }

    public function listCa()
    {
        $contracts = Contract::with('products')->get();
        $results = self::calculateCa($contracts);
        return view('product.ca', compact('contracts', 'results'));
    }

    public function calculateCa($contracts)
    {
        $ttx=0;
        $arrDescContract=[];
        $arrDescPrice=[];
        foreach ($contracts as $contract) {
            $arrProduct=[];
            $arrPrice=[];
            foreach ($contract->products as $product) {
                    $productTT = (float)$product->price * (float)$product->pivot->quantity;
                    array_push($arrProduct, $product->pivot->quantity." ".$product->name." : ".$productTT);
                    array_push($arrPrice, $productTT);
                    $ttx=$ttx + $productTT;
            }
            if (!empty($arrProduct)) {
                array_push($arrDescContract, $arrProduct);
                array_push($arrDescPrice, array_sum($arrPrice));
            }
        }
        return array($ttx, $arrDescContract, $arrDescPrice);
    }

    public function calculateTTX($products)
    {
        $arrPriceProduct = [];
        $ttx=0;
        foreach ($products as $product) {
            $productTT = (float)$product->price * (float)$product->pivot->quantity;
            array_push($arrPriceProduct, $productTT);
            $ttx=$ttx+$productTT;
        }
        return([$arrPriceProduct, $ttx]);
    }
}
