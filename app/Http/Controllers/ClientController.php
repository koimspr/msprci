<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllClient()
    {
        return Client::all();
    }

    /**
     * Get all Client informations for the connected User
     *
     * @return \Illuminate\Htpp\Response
     */
    public function getAllClientForConnectedUser()
    {
        return User::find(Auth::id())->clients;
    }

    /**
     * Display clients ressources for the connected user
     *
     * @return view
     */
    public function listForConnectedUser()
    {
        $clients = self::getAllClientForConnectedUser();
        return view('client.list', compact('clients'));
    }

    /**
     * Display all clients ressources
     *
     * @return view
     */
    public function listAllClients()
    {
        $clients = self::getAllClient();
        return view('client.list', compact('clients'));
    }
}
