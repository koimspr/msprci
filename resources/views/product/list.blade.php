@php
    use App\User;
@endphp
@extends('layouts.app')

@section('page')
    Description des produits de la commande
@endsection

@section('content')
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ Route('home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ Route('client.list') }}">Liste clients</a></li>
                <li class="breadcrumb-item" ><a href="{{ Route('client.list', [$client_id]) }}">Liste commandes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Liste des produits</li>
            </ol>
          </nav>
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        @if(session()->has('ok'))
                            <div class="alert alert-success alert-dismissable alert-style-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <em class="fa fa-check"></em>{{ session('ok') }}
                            </div>
                        @endif
                            <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table align-middle" aria-describedby="product_table">
                                    <thead>
                                    <tr>
                                        <th id="name">Dénomination</th>
                                        <th id="price">Prix Unitaire</th>
                                        <th id="quantity">Quantité requise</th>
                                        <th id="priceTTX">Prix total Produit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $index => $product)
                                        <tr>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td>{{ $product->pivot->quantity }}</td>
                                            <td>{{ $arrPrice[0][$index] }} €</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="float-right text-uppercase font-weight-bold">Total de la commande : {{ $arrPrice[1] }} €</div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
