@php
    use App\User;
@endphp
@extends('layouts.app')

@section('page')
    CHiffre d'Affaire
@endsection

@section('content')
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ Route('home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Calcul du chiffre d'affaire</li>
            </ol>
          </nav>
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        @if(session()->has('ok'))
                            <div class="alert alert-success alert-dismissable alert-style-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <em class="fa fa-check"></em>{{ session('ok') }}
                            </div>
                        @endif
                        <div id="accordion">
                          <div class="row justify-content-md-center">
                          @foreach ($contracts as $item => $contract)
                        
                            <div class="col-6 col-md-offset-2">
                              <div class="card">
                              <div class="card-header" id="heading{{$item}}">
                                    <h5 class="mb-0">
                                  <button class="btn btn-link font-weight-bold" data-toggle="collapse" data-target="#collapse{{$item}}" aria-expanded="flase" aria-controls="collapse{{$item}}">
                                    Contrat {{ $contract->number }}
                                  </button>
                                </h5>
                              </div>
                          
                              <div id="collapse{{$item}}" class="collapse show" aria-labelledby="heading{{$item}}" data-parent="#accordion">
                                <div class="card-body" style="height: 200px;">
                                    <h4 class="font-weight-bold">Descriptif des produits </h4>
                                    <ul>
                                    @foreach ($results[1][$item] as $price)
                                      <li class="text-capitalize">{{ $price }} € HT</li>
                                    @endforeach
                                  </ul> 
                                </div>
                              </div>
                              <br>
                            <blockquote class="d-flex justify-content-center font-weight-bold">TOTAL HT DU CONTRAT : {{ $results[2][$item] }}€ HT</blockquote> 
                            </div>
                            <br>
                          </div>
                        @endforeach
                        </div>
                    </div>
                    <br>
                    <hr>
                    <br>
                    <h2 class="d-flex justify-content-center font-weight-bold">Calcul du total CA</h2>
                    <br>
                    <div class="table-wrap">
                      <div class="table-responsive">
                          <table class="table align-middle" aria-describedby="product_table">
                              <thead>
                              <tr>
                                  <th id="name">Dénomination</th>
                                  <th id="priceHt">Prix HT</th>
                                  <th id="tva">Tva</th>
                                  <th id="priceTva">Prix TVA</th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($contracts as $item => $contract)
                                  <tr>
                                      <td>{{ $contract->number }}</td>
                                      <td>{{ $results[2][$item] }}</td>
                                      <td>20 %</td>
                                      <td>{{ (float)$results[2][$item]*1.2 }} €</td>
                                  </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
                  <br>
                  <blockquote class="d-flex justify-content-center font-weight-bold">TOTAL HT DU CA : {{ $results[0] }}€</blockquote> 
                  <br>
                  <blockquote class="d-flex justify-content-center font-weight-bold">TOTAL TTC DU CA : {{ (float)$results[0]*1.2 }}€</blockquote> 
                  <br>
                </div>
            </div>
        </div>
    </div>
@endsection
