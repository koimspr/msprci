@extends('layouts.app')
@section('page')
    Dashboard
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Menu all client -->
        <div class="col-xl-4 col-md-6 mb-4">
            <a class="card border-left-success shadow h-100 py-2" href="{{ route('client.listUser') }}">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Menu</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">Accèder à mes client</div>
                  </div>
                  <div class="col-auto">
                    <em class="fas fa-dollar-sign fa-2x text-gray-300"></em>
                  </div>
                </div>
              </div>
            </a>
          </div>

        <!-- Menu client -->
        <div class="col-xl-4 col-md-6 mb-4">
          <a class="card border-left-danger shadow h-100 py-2" href="{{ route('client.list') }}">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Menu</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">Accèder à tous les clients</div>
                </div>
                <div class="col-auto">
                  <em class="fas fa-dollar-sign fa-2x text-gray-300"></em>
                </div>
              </div>
            </div>
         </a>
        </div>

        <!-- Menu client -->
        <div class="col-xl-4 col-md-6 mb-4">
            <a class="card border-left-warning shadow h-100 py-2" href="{{ route('contract.calculCa') }}">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Menu</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">Accèder au chiffre d'affaire</div>
                  </div>
                  <div class="col-auto">
                    <em class="fas fa-dollar-sign fa-2x text-gray-300"></em>
                  </div>
                </div>
              </div>
            </a>
          </div>
    </div>
</div>

@endsection
