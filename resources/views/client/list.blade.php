@php
    use App\User;
@endphp
@extends('layouts.app')

@section('page')
    Liste des clients
@endsection

@section('content')
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Liste des clients</li>
            </ol>
          </nav>
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        @if(session()->has('ok'))
                            <div class="alert alert-success alert-dismissable alert-style-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <em class="fa fa-check"></em>{{ session('ok') }}
                            </div>
                        @endif
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table" aria-describedby="client_table">
                                    <thead>
                                    <tr>
                                        <th id="fiscalName">Raison Social</th>
                                        <th id="city">Ville</th>
                                        <th id="country">Pays</th>
                                        <th id="contractNumber">Numéro de contrat</th>
                                        <th id="created_at">Créer le</th>
                                        <th id="modified_at">Modifier le</th>
                                        <th id="listProduct">Consulter la liste des contrats</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>{{ $client->fiscalName }}</td>
                                            <td>{{ $client->city }}</td>
                                            <td>{{ $client->country }}</td>
                                            <td>{{ $client->contractNumber }}</td>
                                            <td>{{ $client->created_at->format('d/m/Y') }}</td>
                                            <td>{{ $client->updated_at->format('d/m/Y à H:i') }}</td>

                                            <td class>
                                                <a href="{{ URL::route('contract.listClient', [$client->id]) }}" data-toggle="tooltip" data-original-title="Edition">
                                                    <button class="btn btn-warning btn-icon-anim btn-square btn-sm"><em class="fa fa-edit"></em></button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
