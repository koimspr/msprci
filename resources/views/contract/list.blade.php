@php
    use App\User;
@endphp
@extends('layouts.app')

@section('page')
    Liste des contrats
@endsection

@section('content')
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}">Dashboard</a></li>
            @if (isset($client_id))
            <li class="breadcrumb-item"><a href="{{ Route('client.list') }}">Tout les clients</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">Liste des contrats</li>
            </ol>
          </nav>
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        @if(session()->has('ok'))
                            <div class="alert alert-success alert-dismissable alert-style-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <em class="fa fa-check"></em>{{ session('ok') }}
                            </div>
                        @endif
                            <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table" aria-describedby="product_table">
                                    <thead>
                                    <tr>
                                        <th id="name">Identifiant du contrat</th>
                                        <th id="created_at">Créer le</th>
                                        <th id="modified_at">Modifier le</th>
                                        <th id="listProduct">Consulter la liste des produits</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contracts as $contract)
                                        <tr>
                                            <td>{{ $contract->number }}</td>
                                            <td>{{ $contract->created_at->format('d/m/Y') }}</td>
                                            <td>{{ $contract->updated_at->format('d/m/Y à H:i') }}</td>
                                            <td class>
                                                <a href="{{ URL::route('product.list', [$contract->id, $client_id]) }}" data-toggle="tooltip" data-original-title="Edition">
                                                    <button class="btn btn-warning btn-icon-anim btn-square btn-sm"><em class="fa fa-edit"></em></button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
