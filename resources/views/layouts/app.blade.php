<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>KOIMSPR</title>

  <!-- Custom fonts for this template-->

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('dist/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/index.css') }}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ Route('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
          <em class="fas fa-laugh-wink"></em>
        </div>
        <div class="sidebar-brand-text mx-3">KOIMSPR</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{ Route('home') }}">
          <em class="fas fa-fw fa-tachometer-alt"></em>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Client -->
      <li class="nav-item">
        <a class="nav-link" href="{{ route('client.listUser') }}">
          <em class="fas fa-fw fa-clipboard-list"></em>
          <span>Mes Clients</span></a>
      </li>

      <!-- Nav Item - Client all -->
      <li class="nav-item">
        <a class="nav-link" href="{{ route('client.list') }}">
          <em class="fas fa-fw fa-clipboard-list"></em>
          <span>Tous les Clients</span></a>
      </li>

      <!-- Nav Item - CalcCa -->
      <li class="nav-item">
        <a class="nav-link" href="{{ route('contract.calculCa') }}">
          <em class="fas fa-fw fa-calculator"></em>
          <span>Calcul du CA</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
  
          <!-- Begin Page Content -->
          <div class="container-fluid">
  
            <!-- Page Heading -->
            <br>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">@yield('page')</h1>
              <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><em class="fas fa-download fa-sm text-white-50"></em> Generate Report</a>
            </div>
  
            <!-- Content Row -->
            <div class="panel panel-default card-view">
              @yield('content')
            </div>
          </div>
          <!-- /.container-fluid -->
  
        </div>
        <!-- End of Main Content -->
  
 
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <em class="fas fa-angle-up"></em>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('dist/js/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('dist/js/sb-admin-2.min.js') }}"></script>

</body>

</html>
