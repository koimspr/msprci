<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Client;
use App\User;

class ProductTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test route client.listUser
     *
     * @return void
     */
    public function testRouteProductListReturnAView()
    {
        $user = factory(User::class)->create();
        $client_id = 3;
        $contract_id= 1;
        $response = $this->actingAs($user)->get('/product/list/'.$contract_id.'/'.$client_id);
        $response->assertStatus(200);
        $response->assertViewHas('products');
        $response->assertViewHas('arrPrice');
        $response->assertViewHas('client_id');

    }

    public function testRouteProductListRedirectWhenUnauthorized()
    {
        $client_id = 3;
        $contract_id= 1;
        $response = $this->get('/product/list/'.$contract_id.'/'.$client_id);
        $response->assertStatus(302);
    }
}
