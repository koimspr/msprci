<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class ContractTest extends TestCase
{
    use DatabaseTransactions;


    /**
     * Route contract.listClient should return a 200 code
     *
     * @return void
     */
    public function testContractListClientReturnAView()
    {
        $user = factory(User::class)->create();
        $client_id = 1;
        $response = $this->actingAs($user)->get('/contract/listClient/'.$client_id);
        $response->assertStatus(200);
        $response->assertViewHas('contracts');
        $response->assertViewHas('client_id');
    }

    /**
     * Route contract.listClient should return a 302 code
     *
     * @return void
     */
    public function testContractListClientRedirectWhenUnauthorized()
    {
        $client_id = 1;
        $response = $this->get('/contract/listClient/'.$client_id);
        $response->assertStatus(302);
    }

    /**
     * Route contract.listClient should return a 200 code
     *
     * @return void
     */
    public function testContractListCAReturnAView()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/calculCa');
        $response->assertStatus(200);
        $response->assertViewHas('contracts');
        $response->assertViewHas('results');
    }

    /**
     * Route contract.listClient should return a 302 code
     *
     * @return void
     */
    public function testContractListCARedirectWhenUnauthorized()
    {
        $response = $this->get('/calculCa');
        $response->assertStatus(302);
    }
}
