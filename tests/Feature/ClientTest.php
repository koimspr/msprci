<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class ClientTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Route client.list should return a 200 code
     *
     * @return void
     */
    public function testClientListReturnAView()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/client/list');
        $response->assertViewHas('clients');
        $response->assertStatus(200);
    }

    /**
     * Route client.listUser should return a 200 code
     *
     * @return void
     */
    public function testClientListUserReturnAView()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/client/listUser');
        $response->assertViewHas('clients');
        $response->assertStatus(200);
    }

    /**
     * Route client.list should return a 302 code
     *
     * @return void
     */
    public function testClientListRedirectWhenUnauthorized()
    {
        $response = $this->get('/client/list');
        $response->assertStatus(302);
    }

    /**
     * Route client.listUser should return a 302 code
     *
     * @return void
     */
    public function testClientListUserRedirectWhenUnauthorized()
    {
        $response = $this->get('/client/listUser');
        $response->assertStatus(302);
    }
}
