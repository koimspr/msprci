<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Factory model of Client
| Create fake data for Client table
|
*/

$factory->define(Client::class, function (Faker $faker) {
    return [
        'fiscalName' => $faker->company, 
        'city' => $faker->city, 
        'country' => $faker->country, 
        'contractNumber' => $faker->randomNumber(6),
    ];
});
