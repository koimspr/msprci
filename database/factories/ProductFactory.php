<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Factory model of Product
| Create fake data for Product table
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->safeColorName." paint bucket",
        'price' => $faker->randomNumber(2),
    ];
});
