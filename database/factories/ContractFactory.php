<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contract;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Factory model of Product
| Create fake data for Product table
|
*/

$factory->define(Contract::class, function (Faker $faker) {
    return [
        'number' => $faker->swiftBicNumber,
    ];
});
