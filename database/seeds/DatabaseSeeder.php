<?php

use Illuminate\Database\Seeder; 

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * 
     * @return void
     */
    public function run()
    {
        //Seed User table
        $this->call(UsersTableSeeder::class);
        //Seed Product table
        $this->call(ProductTableSeeder::class);
        //Seed Client table
        $this->call(ClientTableSeeder::class);
        //Seed Contract table
        $this->call(ContractTableSeeder::class);
        

        //Get all rows in Client table
        $clients = App\Client::all();
        //Get all rows in Contract table
        $products = App\Product::all();
    
        //Populate pivot table user_client
        App\User::all()->each(function($user) use ($clients) {
            $user->clients()->attach(
                $clients->random(rand(1 ,3))->pluck('id')->toArray()
            );
        });

        //Populate pivot table product_contract_client_product
        App\Contract::all()->each(function($contract) use ($products) {
            $contract->products()->attach(
                $products->random(rand(1 ,3))->pluck('id')->toArray(), ['client_id'=>rand(1, 10), 'quantity'=> rand(1, 150)]
            );
        });
    }
}
